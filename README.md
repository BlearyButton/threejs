# Three.JS project

<br />

## Description

This project entails a home page created using Three.JS and GSAP. This project has been an exercise to get to know both Three.JS, and GSAP.

<br />

### Authors

- Stephanie Bolder

### Build with:

This project was created with [Vite](https://vitejs.dev/).

<br />

### Demo

[Click to see live version](https://github.com/BlearyButton/threejs/blob/main/ThreeJS_demo.gif)